# Iris Recognition

This software was created for Illinois Institute of Technology CS 559 Biometrics class.

Instructions:
    1. Install Python 3.5 and set your PATH variables
    2. Install NumPy for fundamental scientific computing: `pip install numpy`
    3. Install Matplotlib for 2D plotting: `pip install matplotlob`
    4. Install OpenCV for computer vision image processing: `pip install opencv-python`
    5. Store gallery in `gallery/LG2200-2008-03-11_13`
    6. Store first set of probes in `probes/probe1/LG4000-2010-04-27_29` and second set of probes in `probes/probe2/LG2200-2010-04-27_29`
    7. Run `python iris_recognition_runner.py`

To see the templates as they're processed, pass `True` as the second argument of the `create_iris_codes` function.

KNOWN BUGS:
    If Hough circles returns nothing, the template is discarded from consideration.

Copyright 2017 Patrick Mendus, Jean-Rodney Larrieux

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.