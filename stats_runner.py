import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

def save_stats(stats):
    return

def print_stats(stats):
    true_hamming = []
    false_hamming = []
    hamming_d = {}
    print('Printing stats...')
    true_matches = 0
    false_matches = 0
    for stat in stats:
        hamming = float(str(round(stat['hd'], 2)))
        if stat['match']:
            true_hamming.append(hamming)
        else:
            false_hamming.append(hamming)

        #TM
        if stat['accept'] and stat['match']:
            true_matches += 1
            if not hamming in hamming_d:
                hamming_d[hamming] = [1, 0]
            else:
                hamming_d[hamming][0] += 1
        #FM
        if stat['accept'] and not stat['match']:
            false_matches += 1
            if not hamming in hamming_d:
                hamming_d[hamming] = [0, 1]
            else:
                hamming_d[hamming][1] += 1
        

    true = sorted(true_hamming)
    false = sorted(false_hamming)
    mu = 0
    sigma = 1
    num_bins = 'auto'
    n, bins, _ = plt.hist(true, num_bins, normed=1, histtype='step')
    y = mlab.normpdf(bins, mu, sigma)
    plt.plot(bins, y, 'r--')
    n2, bins2, _ = plt.hist(false, num_bins, normed=1, histtype='step')
    y2 = mlab.normpdf(bins2, mu, sigma)
    plt.plot(bins2, y2, 'r--')
    plt.show()

    rocx = []
    rocy = []
    for h in hamming_d:
        rocx.append((hamming_d.get(h)[1])/false_matches)
        rocy.append((hamming_d.get(h)[0])/true_matches)
    
    plt.plot(rocx, rocy, 'ro')
    plt.show()